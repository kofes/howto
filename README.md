# [HOWTO]
 - WSL
 - Clang
 - Google Style Guide
 - linter
 - lcov/gcov
 
 ## WSL
 [Link to source page](https://docs.microsoft.com/en-us/windows/wsl/install-win10)
 Short description:
 1. Open PowerShell as Administrator and run:
```
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
```
2. Restart your computer when prompted.
3. Go to `Microsoft store` (windows application)
4. Search for `Ubuntu` and download it
5. After installation run `Ubuntu` application

## Clang
[Ubuntu]
1. `sudo apt install clang` (install clang if it not installed yet)
2. `clang++ -fsanitize=address -g -Wall -Werror source.cpp [other_sources...] -o main`
3. run application `./main`

[Windows]
[Link to llvm download page](http://releases.llvm.org/download.html) 
1. download clang from [link](http://releases.llvm.org/download.html) (goto `Pre-Built Binaries` section & download Windows (32bit) / Windows (64bit) version)
2. run binary file & install clang
3. open windows terminal / git-bash execute `clang++ -fsanitize=address -g -Wall -Werror source.cpp [other_sources...] -o main`
4. run application `main.exe`

## Google Style Guide
[Ubuntu]
1. `sudo apt install clang-format` (install clang-format if it not installed yet)
2. `clang-format -style=Google source.cpp > output_source.cpp`

[Windows]
1. `clang-format -style=Google source.cpp > output_source.cpp`

## linter
Download cpplint.py from [cats](https://imcs.dvfu.ru/cats/main.pl?f=wiki;name=cpp-course-2019)
[Ubuntu]
1. `sudo apt install python` (if it necessary)
2. `python path/to/cpplint.py source.cpp`

[Windows]
1. Install [python](https://www.python.org/downloads/windows/) directly or through [anaconda](https://www.anaconda.com/distribution/)
2. `python path/to/cpplint.py source.cpp`

## lcov [linux only]
1.  `g++ -fprofile-arcs -ftest-coverage source1.cpp [sourceN.cpp ...] -o executable_filename`
2.  `./executable_filename`
3.  `lcov -c --directory . --output-file main_cov.info`
4.  `genhtml main_cov.info --output-directory out`